__author__ = 'Daniel Lins'

# Modules
from flask import Flask
from flask.ext.sqlalchemy import SQLAlchemy
from flask.ext.restful import Api
from flask.ext.httpauth import HTTPBasicAuth

# Application Initializations
app = Flask(__name__)
api = Api(app)
auth = HTTPBasicAuth()
app.config.from_object('config')
db = SQLAlchemy(app)


from app import views
from app.resources import models
from app.resources.resources import *
